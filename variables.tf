# Customize the variables as you see fit
variable "aws_region" { default = "us-gov-west-1" }
variable "stackrox_ami" { default = "ami-d35f62b2" }
variable "master_subnet" { default = "subnet-844eb9cd" }
variable "node_subnets" { default = ["subnet-52057a0b", "subnet-689f740f", "subnet-844eb9cd"] }
variable "master_size" { default = "m5.xlarge" }
variable "node_size" { default = "m5.xlarge" }
variable "security_group" { default = "sg-b2ad70cb" }
variable "ec2_keypair" { default = "chad-ingle-gov-west" }
variable "ssh_private_key" { default = "~/.ssh/chad-ingle-gov-west.pem" }
variable "ssh_user" { default = "centos" }
variable "master_storage" { default = 50 }
variable "node_storage" { default = 50 }
variable "number_of_nodes" { default = 4 }