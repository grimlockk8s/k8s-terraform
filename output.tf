output "master_public_ip" {
  description = "Master Public IP"
  value       = aws_instance.master.public_ip
}

output "new_token" {
  value = "${module.kubeadm-token.token}"
}