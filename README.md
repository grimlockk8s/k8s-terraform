## K8S Cluster Deployment

This repo is designed to deploy a master and a number of nodes to create a k8s cluster.

## Deploy K8S Cluster

Clone the repo:

```
git clone git@gitlab.com:grimlockk8s/k8s-terraform.git
cd ./k8s-terraform
```

Edit the variables.tf file for your deployment.  Specifically your keys, subnet, security group, etc.
```
vim variables.tf
```

Export your credentials and region OR set your credentials using the .aws/credentials:

```
export TF_VAR_aws_region=us-gov-west-1
export TF_VAR_aws_access_key_id=asdfghjkl123456789
export TF_VAR_aws_secret_access_key=hfjdlkshafkdlshl57489375/hfkjdshi
```

Run terraform init if you haven't yet on your workstation:

```
terrafor init
```

Run terraform plan to verify the credentials and variables.tf are set:

```
terraform plan
```

Deploy:

```
terraform apply
```

## Destroy
Run terraform destroy:

```
terraform destroy -force
```