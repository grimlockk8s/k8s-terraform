provider "aws" {
  region = var.aws_region
}

module "kubeadm-token" {
  source = "github.com/scholzj/terraform-kubeadm-token"
}

resource "aws_instance" "master" {
  ami                         = var.stackrox_ami
  instance_type               = var.master_size
  subnet_id                   = var.master_subnet
  vpc_security_group_ids      = [var.security_group]
  associate_public_ip_address = true
  key_name                    = var.ec2_keypair
  ebs_block_device {
    device_name           = "/dev/sda1"
    volume_size           = var.master_storage
    volume_type           = "gp2"
    delete_on_termination = true
  }

  provisioner "file" {
    source = "yamls/calico.yaml"
    destination = "~/calico.yaml"
    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
      host        = self.public_ip
    }
  }

  provisioner "file" {
    source = "docker-install.sh"
    destination = "~/docker-install.sh"
    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
      host        = self.public_ip
    }
  }

  provisioner "remote-exec" {
    script = "k8s-dependencies.sh"
    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
      host        = self.public_ip
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x ~/docker-install.sh",
      "sudo ~/docker-install.sh",
      "sudo systemctl enable docker",
			"sudo systemctl start docker",
      "sudo usermod -a -G docker centos",
      "export KUBE_COMMAND='sudo kubectl --kubeconfig /etc/kubernetes/admin.conf'",
      "sudo kubeadm init --kubernetes-version \"1.18.4\" --token ${chomp(module.kubeadm-token.token)}",
      "sudo mkdir -p /root/.kube",
      "sudo cp -i /etc/kubernetes/admin.conf /root/.kube/config",
      "sudo chown $(id -u):$(id -g) /root/.kube/config",
      "export MASTERNODE=`$KUBE_COMMAND get node --selector='node-role.kubernetes.io/master' --output=jsonpath={.items..metadata.name}`",
      "$KUBE_COMMAND taint nodes $MASTERNODE node-role.kubernetes.io/master:NoSchedule-",
      "$KUBE_COMMAND apply -f ~/calico.yaml"
    ]
    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
      host        = self.public_ip
    }
  }

  tags = {
    Name = "K8S Master"
  }
}

resource "aws_instance" "node" {
  count                       = var.number_of_nodes
  ami                         = var.stackrox_ami
  instance_type               = var.node_size
  subnet_id                   = element(var.node_subnets, count.index + var.number_of_nodes % var.number_of_nodes)
  vpc_security_group_ids      = [var.security_group]
  associate_public_ip_address = true
  key_name                    = var.ec2_keypair
  ebs_block_device {
    device_name           = "/dev/sda1"
    volume_size           = var.node_storage
    volume_type           = "gp2"
    delete_on_termination = true
  }

  provisioner "file" {
    source = "docker-install.sh"
    destination = "~/docker-install.sh"
    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
      host        = self.public_ip
    }
  }
  
  provisioner "remote-exec" {
    script = "k8s-dependencies.sh"
    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
      host        = self.public_ip
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x ~/docker-install.sh",
      "sudo ~/docker-install.sh",
      "sudo systemctl enable docker",
			"sudo systemctl start docker",
      "sudo usermod -a -G docker centos",
      "echo ${join("", ["Executing: sudo kubeadm join ", "${aws_instance.master.private_ip}", ":6443 --token ", "${chomp(module.kubeadm-token.token)}", " --discovery-token-unsafe-skip-ca-verification"])}",
      join("", ["sudo kubeadm join ", "${aws_instance.master.private_ip}", ":6443 --token ", "${chomp(module.kubeadm-token.token)}", " --discovery-token-unsafe-skip-ca-verification"])
    ]
    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
      host        = self.public_ip
    }
  }

  tags = {
    Name = "K8S Node ${count.index + 1}"
  }
}
