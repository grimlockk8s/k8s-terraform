#!/bin/bash

# Patch the server
echo "Patching the server..."
sudo yum update -y
echo "Patching complete."

# Install tools
echo "Installing common tools..."
sudo yum install iscsi-initiator-utils wget mlocate git curl openssl telnet unzip bind-utils python3 python3-pip tcpdump -y
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
wget https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
sudo mv jq-linux64 /usr/sbin/jq
sudo chmod +x /usr/sbin/jq
sudo ln -s /usr/local/bin/aws /usr/bin/aws
sudo pip-3 install boto3 pytz pyyaml requests docker docker-compose docker[tls] selinux
echo "Tools install complete."
echo "Installing k8s..."
cat <<EOF > kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
sudo mv kubernetes.repo /etc/yum.repos.d/kubernetes.repo
cat <<EOF > k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
vm.swappiness = 0
vm.overcommit_memory = 1
net.ipv4.ip_local_port_range=1024 65000
net.core.somaxconn = 10000
net.ipv4.tcp_tw_reuse=1
net.ipv4.tcp_fin_timeout=15
net.core.somaxconn=4096
net.core.netdev_max_backlog=4096
net.core.rmem_max=16777216
net.core.wmem_max=16777216
net.ipv4.tcp_max_syn_backlog=20480
net.ipv4.tcp_max_tw_buckets=400000
net.ipv4.tcp_no_metrics_save=1
net.ipv4.tcp_rmem=4096 87380 16777216
net.ipv4.tcp_syn_retries=2
net.ipv4.tcp_synack_retries=2
net.ipv4.tcp_wmem=4096 65536 16777216
net.ipv4.neigh.default.gc_thresh1=8096
net.ipv4.neigh.default.gc_thresh2=12288
net.ipv4.neigh.default.gc_thresh3=16384
net.ipv4.tcp_keepalive_time=600
net.ipv4.ip_forward=1
fs.may_detach_mounts=1
fs.inotify.max_user_instances=8192
fs.inotify.max_user_watches=1048576
EOF
sudo mv k8s.conf /etc/sysctl.d/k8s.conf
sudo sysctl --system
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
sudo yum install -y kubelet-1.18.4 kubeadm-1.18.4 kubectl-1.18.4 –-disableexcludes=kubernetes
sudo systemctl enable --now kubelet
echo "K8S install complete."
